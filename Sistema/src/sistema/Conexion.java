/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

//import java.sql.Connection;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
//import java.sql.DriverManager;

/**
 *
 * @author Fernando
 */
public class Conexion {
     
    String driver = "com.mysql.jdbc.Driver";
    String user = "root";
    String pass = "";
    String url= "jdbc:mysql://localhost:3306/db_institucion";
    String host="localhost";
    String port="3306";
    String db_name="db_institucion";
    String username="root";
    String password="";
    //String password="jeremias12345";
               
    public Connection conexion(){
        Connection connection = null;
        
        try{
            Class.forName(driver);
            connection = (Connection) DriverManager.getConnection(url,user,pass);
            if (connection != null) {
                System.out.println("Connection OK");
            }else{
                System.out.println("Connection failed");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return connection;
    }
    
    public static void main(String []args){
        Conexion cc = new Conexion();
        Connection cn = cc.conexion(); 
    }
   
}
