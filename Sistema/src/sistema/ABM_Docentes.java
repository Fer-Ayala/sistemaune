package sistema;

import javax.swing.table.DefaultTableModel;
import com.mysql.jdbc.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Fernando
 */
public class ABM_Docentes extends javax.swing.JFrame {

    
    public ABM_Docentes() {
        initComponents();
        mostrarDatos("","");
        this.setLocationRelativeTo(null);
    }
    
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        opciones_popupMenu = new javax.swing.JPopupMenu();
        editar_popup = new javax.swing.JMenuItem();
        borrar_popup = new javax.swing.JMenuItem();
        panel1 = new javax.swing.JPanel();
        nombre_label = new javax.swing.JLabel();
        cedula_label = new javax.swing.JLabel();
        nombre_textField = new javax.swing.JTextField();
        cedula_textField = new javax.swing.JTextField();
        contacto_label = new javax.swing.JLabel();
        contacto_textField = new javax.swing.JTextField();
        apellido_label = new javax.swing.JLabel();
        tipoDoc_label = new javax.swing.JLabel();
        contacto_label1 = new javax.swing.JLabel();
        apellido_textField = new javax.swing.JTextField();
        tipoDoc_comboBox = new javax.swing.JComboBox<>();
        email_textField = new javax.swing.JTextField();
        tituloCrear_label = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        panel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaAlumnos = new javax.swing.JTable();
        panel2 = new javax.swing.JPanel();
        guardar_button = new javax.swing.JButton();
        editar_button = new javax.swing.JButton();
        cancelar_button1 = new javax.swing.JButton();
        cancelar_button2 = new javax.swing.JButton();
        panel3 = new javax.swing.JPanel();
        buscar_button = new javax.swing.JButton();
        busqueda_comboBox = new javax.swing.JComboBox<>();
        busqueda_textField = new javax.swing.JTextField();
        tituloBuscar_label = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();

        editar_popup.setText("Editar");
        editar_popup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editar_popupActionPerformed(evt);
            }
        });
        opciones_popupMenu.add(editar_popup);

        borrar_popup.setText("Borrar");
        borrar_popup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                borrar_popupActionPerformed(evt);
            }
        });
        opciones_popupMenu.add(borrar_popup);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        panel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombre_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        nombre_label.setText("Nombres:");

        cedula_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cedula_label.setText("N° de Cédula:");

        nombre_textField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombre_textFieldActionPerformed(evt);
            }
        });

        contacto_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        contacto_label.setText("Contacto:");

        apellido_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        apellido_label.setText("Apellidos:");

        tipoDoc_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tipoDoc_label.setText("Tipo doc.");

        contacto_label1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        contacto_label1.setText("E-mail:");

        apellido_textField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                apellido_textFieldActionPerformed(evt);
            }
        });

        tipoDoc_comboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        tipoDoc_comboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tipoDoc_comboBoxActionPerformed(evt);
            }
        });

        tituloCrear_label.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        tituloCrear_label.setText("Crear Docente");

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addComponent(contacto_label)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(email_textField))
                            .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addComponent(nombre_label)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(nombre_textField, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addComponent(cedula_label)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(cedula_textField, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(apellido_label, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(tipoDoc_label, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addComponent(contacto_label1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(apellido_textField)
                            .addComponent(tipoDoc_comboBox, 0, 190, Short.MAX_VALUE)
                            .addComponent(contacto_textField))
                        .addGap(80, 80, 80))
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(tituloCrear_label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42))))
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tituloCrear_label)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombre_label)
                    .addComponent(nombre_textField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(apellido_label)
                    .addComponent(apellido_textField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cedula_textField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cedula_label)
                        .addComponent(tipoDoc_comboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tipoDoc_label)))
                .addGap(18, 18, 18)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(contacto_label)
                    .addComponent(email_textField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(contacto_label1)
                    .addComponent(contacto_textField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        panel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        tablaAlumnos.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        tablaAlumnos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tablaAlumnos.setComponentPopupMenu(opciones_popupMenu);
        jScrollPane2.setViewportView(tablaAlumnos);
        tablaAlumnos.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout panel4Layout = new javax.swing.GroupLayout(panel4);
        panel4.setLayout(panel4Layout);
        panel4Layout.setHorizontalGroup(
            panel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        panel4Layout.setVerticalGroup(
            panel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(260, 260, 260))
        );

        panel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        guardar_button.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        guardar_button.setText("Guardar");
        guardar_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardar_buttonActionPerformed(evt);
            }
        });

        editar_button.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        editar_button.setText("Editar");
        editar_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editar_buttonActionPerformed(evt);
            }
        });

        cancelar_button1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cancelar_button1.setText("CV");
        cancelar_button1.setToolTipText("");
        cancelar_button1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelar_button1ActionPerformed(evt);
            }
        });

        cancelar_button2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cancelar_button2.setText("Cancelar");
        cancelar_button2.setToolTipText("");
        cancelar_button2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelar_button2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editar_button, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(guardar_button, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cancelar_button2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cancelar_button1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(guardar_button)
                .addGap(13, 13, 13)
                .addComponent(editar_button)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cancelar_button2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cancelar_button1))
        );

        panel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        buscar_button.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        buscar_button.setText("Buscar");
        buscar_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscar_buttonActionPerformed(evt);
            }
        });

        busqueda_comboBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        busqueda_comboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nombre", "Cedula" }));

        tituloBuscar_label.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        tituloBuscar_label.setText("Buscar Docente");

        javax.swing.GroupLayout panel3Layout = new javax.swing.GroupLayout(panel3);
        panel3.setLayout(panel3Layout);
        panel3Layout.setHorizontalGroup(
            panel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel3Layout.createSequentialGroup()
                        .addComponent(busqueda_comboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(busqueda_textField)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buscar_button)
                        .addGap(34, 34, 34))
                    .addGroup(panel3Layout.createSequentialGroup()
                        .addComponent(tituloBuscar_label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator3))))
        );
        panel3Layout.setVerticalGroup(
            panel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tituloBuscar_label)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buscar_button)
                    .addComponent(busqueda_comboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(busqueda_textField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, 578, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(panel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel4, javax.swing.GroupLayout.PREFERRED_SIZE, 204, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void guardar_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardar_buttonActionPerformed
        if(nombre_textField.getText().equals("")){
           JOptionPane.showMessageDialog(rootPane, "No puedes guardar un valor vació");
        }else{
            ca.guardar(nombre_textField.getText(), cedula_textField.getText(), contacto_textField.getText()); 
            mostrarDatos("","");
            nombre_textField.setText("");
            cedula_textField.setText("");
            contacto_textField.setText("");
        }
    }//GEN-LAST:event_guardar_buttonActionPerformed

    private void buscar_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscar_buttonActionPerformed
        String valor = busqueda_textField.getText();
        String filtro = busqueda_comboBox.getSelectedItem().toString();
        if(filtro.equals("Nombre")){
            mostrarDatos(valor, "");
        }else{
            mostrarDatos("", valor);
        }
    }//GEN-LAST:event_buscar_buttonActionPerformed

    private void editar_popupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editar_popupActionPerformed
        int filaSeleccionada = tablaAlumnos.getSelectedRow();
        nombre_textField.setText(tablaAlumnos.getValueAt(filaSeleccionada, 1).toString().trim());
        cedula_textField.setText(tablaAlumnos.getValueAt(filaSeleccionada, 2).toString().trim());
        contacto_textField.setText(tablaAlumnos.getValueAt(filaSeleccionada, 3).toString().trim());
        idEditar = Integer.parseInt(tablaAlumnos.getModel().getValueAt(filaSeleccionada, 4).toString());
        editar_button.setVisible(true);
        //carrera_button.setVisible(true);
        guardar_button.setVisible(false);
    }//GEN-LAST:event_editar_popupActionPerformed

    private void borrar_popupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_borrar_popupActionPerformed
        int filaSeleccionada = tablaAlumnos.getSelectedRow();
       
        idEditar = Integer.parseInt(tablaAlumnos.getModel().getValueAt(filaSeleccionada, 3).toString());
        ca.delete(idEditar);
        mostrarDatos("","");
        idEditar = null;
    }//GEN-LAST:event_borrar_popupActionPerformed

    private void apellido_textFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_apellido_textFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_apellido_textFieldActionPerformed

    private void nombre_textFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombre_textFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nombre_textFieldActionPerformed

    private void editar_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editar_buttonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editar_buttonActionPerformed

    private void tipoDoc_comboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tipoDoc_comboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tipoDoc_comboBoxActionPerformed

    private void cancelar_button1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelar_button1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cancelar_button1ActionPerformed

    private void cancelar_button2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelar_button2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cancelar_button2ActionPerformed

    // Own functions ***********************************************************
    void mostrarDatos(String nombre, String cedula){
        
        editar_button.setVisible(false);
        //carrera_button.setVisible(false);
        
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("Contador");
        modelo.addColumn("Nombre");
        modelo.addColumn("Cedula");
        modelo.addColumn("Contacto");
        modelo.addColumn("Id");
        
        tablaAlumnos.setModel(modelo);
       
        String[] datos = new String[5];
        try{
            ResultSet rs = ca.buscar(nombre, cedula);
            int i = 1;
            while(rs.next()){
                datos[0] = String.valueOf(i);
                datos[1] = rs.getString(2);
                datos[2] = rs.getString(3);
                datos[3] = rs.getString(4);
                datos[4] = rs.getString(1);
                                
                modelo.addRow(datos);
                i++;
            }
            tablaAlumnos.setModel(modelo);
            TableColumnModel tcm = tablaAlumnos.getColumnModel();
            tcm.removeColumn(tcm.getColumn(4));
            
        }catch(SQLException ex){
            System.out.println("Algo falló"+ex);
        }
        
        editar_button.setVisible(false);
    }
        
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ABM_Docentes().setVisible(true);
            }
        });
    }
    //Own Variables
    public ConexionAlumnos ca = new ConexionAlumnos();
//    
    public Integer idEditar = 0;
    
    public Integer idCarrera = 0;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel apellido_label;
    private javax.swing.JTextField apellido_textField;
    private javax.swing.JMenuItem borrar_popup;
    private javax.swing.JButton buscar_button;
    private javax.swing.JComboBox<String> busqueda_comboBox;
    private javax.swing.JTextField busqueda_textField;
    private javax.swing.JButton cancelar_button1;
    private javax.swing.JButton cancelar_button2;
    private javax.swing.JLabel cedula_label;
    private javax.swing.JTextField cedula_textField;
    private javax.swing.JLabel contacto_label;
    private javax.swing.JLabel contacto_label1;
    private javax.swing.JTextField contacto_textField;
    private javax.swing.JButton editar_button;
    private javax.swing.JMenuItem editar_popup;
    private javax.swing.JTextField email_textField;
    private javax.swing.JButton guardar_button;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel nombre_label;
    private javax.swing.JTextField nombre_textField;
    private javax.swing.JPopupMenu opciones_popupMenu;
    private javax.swing.JPanel panel1;
    private javax.swing.JPanel panel2;
    private javax.swing.JPanel panel3;
    private javax.swing.JPanel panel4;
    private javax.swing.JTable tablaAlumnos;
    private javax.swing.JComboBox<String> tipoDoc_comboBox;
    private javax.swing.JLabel tipoDoc_label;
    private javax.swing.JLabel tituloBuscar_label;
    private javax.swing.JLabel tituloCrear_label;
    // End of variables declaration//GEN-END:variables
}
